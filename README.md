# (IMPORTANT) The future of Orbit

During the past 2 months, I've been planning out the future of Orbit due to various reasons that go from security/privacy to "leaving a legacy that anyone can contribute to", and this is why after a really long time I'm coming back to the "cockpit" to start working on Orbit again, but not with out some changes.

I've layed out some goals and basics for the next version of Orbit (v2) in my Twitter (see [February's](https://nitter.net/lordvesconte/status/1490508350613766145#m) and [April's](https://nitter.net/lordvesconte/status/1510362689079545858#m) post about it) for the past 2 months, there are not a lot of mayor changes outside branding and codebase-related stuff that doesn't really affect the end user but here are some of the proposed ideas that I've been planning on implementing for the next update:

- Complete branding overhaul to reflect Orbit's new focus on simplicity and stability, alongside the "Our browser, not his" slogan.
- New stylesheets and UI resources to make the browser look like something that wasn't made 15 years ago, both for user convenience and to flex that Tobin can't make his browser look nice.
- Self-hosted SearX instance set as the default search engine for Orbit, that will probably hosted in Mexico too!
- Proper mirrors in reputable CDNs (SourceForge?) alongside distribution of pre-compiled binaries for Linux (fuck off Wintards)
- Forking the current version of Pale Moon to start development "with a clean start" and with up-to-date resources.
- Have the browser come hardened out-of-the-box with all the recommended hardening measures for Pale Moon so that end users don't have to worry about it.
- Bundling an Adblocker (UOrigin?), an User Agent Switcher and Decentraleyes pre-installed (this might not be done due to licensing reasons and because users may not like this decision, so its all up to debate /shrug)
- More things that I'm too retarded to remember as I'm writing this at 10pm and I should have been asleep like 3 hours ago!

That's all from me until I manage to make a working prototype to release and start proper development on, until then, see you.

`- Kris`

# Orbit Navigator

This is the source code for the Orbit Navigator web browser, an ultra-lightweight and really-fucking-simple internet navigator. 

The source tree is laid out in a "comm-central" style configuration where only the code specific to our beloved Orbit is kept in this repository.

The shared Unified XUL Platform-Fixed source code is referenced here as a git submodule contained in the `platform/` directory and is required to build the application.

Note: I have removed all proprietary Pale Moon branding. As such, this repository does not violate any copyrights imposed by Moonchild Productions. Any copyright claims submitted against this repo are false. In addition, the rights to the MPL licensed Pale Moon code cannot be revoked from me as it is very clear which commit corresponds to each Linux binary.

## Changes compared to Pale Moon (codebase from where Orbit is based off):
* Branding changed from Pale Moon to Orbit Navigator. (Did you know: the word "Navigator" makes Tobin seethe)
* Restore dual GUID system/Firefox addons support
* Revert removal of ability to set extension update background URL (in case someone else starts their own addon site)
* Restore support for system libvpx
* Restore support for system sqlite (I don't recommend using this.)
* Restore support for system NSPR/NSS
* Restore support for system ICU (this breaks on newer ICU versions)
* Removal of Tobin's precious directive 4 (you can use official branding and system libs at the same time now)

## FAQ

* Is this Male Poon?
    * Yes, but with a different name and a different maintainer.
* Doesn't this violate Moonchild Productions' branding and copyrights?
    * No. Moonchild Productions owns (but has not legally registered) the copyright for Moonchild Productions and Pale Moon. He does not own the copyright for Orbit Navigator or anything related to it. I am not using any of Pale Moon's original assets (logo, fonts and whatever the fuck), I made the logo myself by spending 10 minutes on Inkscape drawing random shit. As such, I am not in violation of any branding. I'll be happy to go to court to argue this.
* Are there any binaries available?
    * As of now, No. But there are some binaries from the "Male Poon" era at http://pajeet.tech/malepoon/, you might be interested in that although they're nolonger supported by me.
* What will happen if you get suspended from Gitlab?
    * I'll move the project to [lolcat's git instance](https://git.lolcat.ca/), more info about it below.
* Is there an official mirror of this repo somewhere?
    * Yes, you can find it at https://git.lolcat.ca/kris/orbit. Incase shit goes down, this will be the new repo of Orbit.
* Will you provide any sort of Linux package?
    * Probably not, Orbit is more of a "archive" project while Manchild gets his repo back, I may make some RPM packages but maybe just for personal use.

## Code of Conduct
* There is no "Code of Conduct" (read why [here](https://libreboot.org/news/rms.html#coraline-ada-ehmke), The only thing that you have to do is follow the rule below and promise not to seethe because I forked your shit.
    * In addition to the previous point, by using and/or contributing to this software, you agree not to open Gitlab/Gitea issues for bullshit licensing violations or any other autistic reasons.

## Credits:
* MoonchildProductions, Pale Moon/UXP contributors for forking Firefox and making the wonderful Pale Moon Browser and UXP platform.
* Matt Tobin for continuing to complain about GitHub forks he doesn't like and for throwing a bogus DMCA claim on the old repository.


## Notes:

As of now, Orbit lacks maintainers and people who can actually code shit into it.

### shitty letters/things to say:

<details>
  <summary>To Manchild:</summary>
  I made this repo just so you can come back with an already re-branded of Male Poon with some stuff added in. You'll always be welcome to maintain this and do whatever you want, just email me @ citizensixtynine@protonmail.com
</details>

<details>
    <summary>To Moonchild/Tobin:</summary>
    I'll be here whenever you want to send me legal threats or DMCA takedowns, I'll be happy to contact my lawyer. ;)
</details>