# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

installer:
	@$(MAKE) -C orbit/installer installer

package:
	@$(MAKE) -C orbit/installer make-archive

l10n-package:
	@$(MAKE) -C orbit/installer make-langpack

mozpackage:
	@$(MAKE) -C orbit/installer

package-compare:
	@$(MAKE) -C orbit/installer package-compare

stage-package:
	@$(MAKE) -C orbit/installer stage-package make-buildinfo-file

install::
	@$(MAKE) -C orbit/installer install

clean::
	@$(MAKE) -C orbit/installer clean

distclean::
	@$(MAKE) -C orbit/installer distclean

source-package::
	@$(MAKE) -C orbit/installer source-package

upload::
	@$(MAKE) -C orbit/installer upload

source-upload::
	@$(MAKE) -C orbit/installer source-upload

hg-bundle::
	@$(MAKE) -C orbit/installer hg-bundle

l10n-check::
	@$(MAKE) -C orbit/locales l10n-check

ifdef ENABLE_TESTS
# Implemented in testing/testsuite-targets.mk

mochitest-browser-chrome:
	$(RUN_MOCHITEST) --browser-chrome
	$(CHECK_TEST_ERROR)

mochitest:: mochitest-browser-chrome

.PHONY: mochitest-browser-chrome

mochitest-metro-chrome:
	$(RUN_MOCHITEST) --metro-immersive --browser-chrome
	$(CHECK_TEST_ERROR)


endif
