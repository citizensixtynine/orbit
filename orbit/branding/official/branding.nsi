# NSIS branding defines for official release builds.

# BrandFullNameInternal is used for some registry and file system values
# instead of BrandFullName and typically should not be modified.
!define BrandFullNameInternal "Orbit Navigator"
!define CompanyName           "Kris Pineda"
!define URLInfoAbout          "https://github.com/CITIZENSIXTYNINE/orbit"
!define URLUpdateInfo         "https://github.com/CITIZENSIXTYNINE/orbit/releases"
!define HelpLink              "https://github.com/CITIZENSIXTYNINE/orbit/issues"
!define URLSystemRequirements "https://github.com/CITIZENSIXTYNINE/orbit"
