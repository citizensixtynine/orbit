# Note about branding

Please take note that these branding files are **not protected by copyright and trademarks**. 
Take notice of the conditions set forth in `LICENSE` before building and distributing
any software using _any_ of these branding files in whole or in part. This includes any
unique identifiers that may be present in preference files.

All the branding is licensed under the Unlicense.

Go have fun kiddo.